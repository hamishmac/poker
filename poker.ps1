 # Set up -verbose parameter
 Param ([switch]$verbose)
 if($verbose)
 {
    $VerbosePreference = "continue"
 }

# Formats an array of cards for scoring comparison
# Example: 9,4,5,6,7 = 09 04 05 06 07
function FormatForScoring {

    param( $ScoreCardArray )

    $ScoreString = @($ScoreCardArray | ForEach-Object { $_.ToString("00") }) -join " "

    return $ScoreString
}

function ScoreHand {
    param( $HandCards )

    # Set up the return object, with a description and scoring string
    $ReturnObj = @{}
    $ReturnObj.Desc = ""
    $ReturnObj.ScoreString = ""

    # Array of possible faces
    $FaceArray = @("2","3","4","5","6","7","8","9","T","J","Q","K","A")

    # Array of possible suits
    $SuitArray = @("H", "D", "S", "C")

    # Validate card suits, faces
    if(($HandCards | where { $FaceArray -notcontains $_.substring(0,1) }) `
        -or ($HandCards | where { $SuitArray -notcontains $_.substring(1,1) })
    )
    {
        $ReturnObj.Desc = "Invalid hand"
        $ReturnObj.ScoreString = "00"
        Return $ReturnObj
    }

    # Count suits by grouping by suit / second letter
    $GroupedCards = $HandCards | Group-Object -Property { $_.substring(1,1) }

    # If only one suit, we have a flush
    if($GroupedCards.Name.Length -eq 1)
    {
        $isFlush = $true
    } else
    {
        $isFlush = $false
    }

    # Create scored cards, for easy sorting and comparison
    $ScoredCards = @()
    foreach ($ThisCard in $HandCards)
    {
        # Assign scores
        $ScoredCards += [array]::indexof($FaceArray,$ThisCard.substring(0,1))
    }

    # Sorting is now easy - sort descending as high pairs etc should come first
    $SortedScoredCards = $ScoredCards | Sort-Object -Descending

    # Check for a Straight
    $isStraight = $true
    for ($count=1; $count -le ($SortedScoredCards.Length-1); $count++)
    {
        if (($SortedScoredCards[$count-1]) -ne $SortedScoredCards[$count]+1)
        {
            $isStraight = $false
        }
    }

    # Royal straight flush = if isFlush and isStraight and high card is an Ace
    if ($isFlush -and $isStraight -and $SortedScoredCards[0] -eq [array]::indexof($FaceArray,"A"))
    {
        $ReturnObj.Desc = "Royal straight flush"
        $ReturnObj.ScoreString = FormatForScoring(@(10, $SortedScoredCards[0]))
        Return $ReturnObj
    }

    # Straight flush = if isFlush and isStraight
    if ($isFlush -and $isStraight)
    {
        $ReturnObj.Desc = "Straight flush"
        $ReturnObj.ScoreString = FormatForScoring(@(9, $SortedScoredCards[0]))
        Return $ReturnObj
    }

    # Analyze cards for pairs, triples, quads, etc

    # Set up variables
    $RemainingCards = @()
    $isOnePair = $false
    $isTwoPair = $false
    $isThreeKind = $false
    $isFourKind = $false

    # Group by faces
    $FaceGroup = $SortedScoredCards | Group-Object
    
    # For each face
    foreach ($ThisFace in $FaceGroup)
    {
        # Look at the number of cards of this face
        switch ($ThisFace.Count)
        {
            1 {
                # Singles are added to remaining cards for tiebreaking
                $RemainingCards += [int]$ThisFace.Name
            }
            2 {
                # Pairs
                # Check if One Pair has already been found
                if ($isOnePair -eq $true )
                {
                    # Two pair
                    $isTwoPair = $true
                    $TwoPairCard = [int]$ThisFace.Name
                } else {
                    # One pair
                    $isOnePair = $true
                    $OnePairCard = [int]$ThisFace.Name
                }
            }
            3 {
                # Triples
                $isThreeKind = $true
                $TripleCard = [int]$ThisFace.Name
            }
            4 {
                # Quads
                $isFourKind = $true
                $QuadCard = [int]$ThisFace.Name
            }
        }

    }

    # Four of a kind = if isFourKind
    if ($isFourKind)
    {
        $ReturnObj.Desc = "Four of a kind"
        $ReturnObj.ScoreString = FormatForScoring(@(8, $QuadCard) + $RemainingCards)
        Return $ReturnObj
    }

    # Full house = if isThreeKind and isOnePair
    if ($isThreeKind -and $isOnePair)
    {
        $ReturnObj.Desc = "Full house"
        $ReturnObj.ScoreString = FormatForScoring(@(7, $TripleCard, $OnePairCard))
        Return $ReturnObj
    }

    # Flush = if isFlush 
    if ($isFlush)
    {
        $ReturnObj.Desc = "Flush"
        $ReturnObj.ScoreString = FormatForScoring(@(6, $SortedScoredCards[0]))
        Return $ReturnObj
    }

    # Straight, if isStraight
    if ($isStraight)
    {
        $ReturnObj.Desc = "Straight"
        $ReturnObj.ScoreString = FormatForScoring(@(5, $SortedScoredCards[0]))
        Return $ReturnObj
    }

    # Three of a kind = isThreeKind
    if ($isThreeKind)
    {
        $ReturnObj.Desc = "Three of a kind"
        $ReturnObj.ScoreString = FormatForScoring(@(4, $TripleCard) + $RemainingCards)
        Return $ReturnObj
    }

    # Two pair = isTwoPair
    if ($isTwoPair)
    {
        $ReturnObj.Desc = "Two pair"
        $ReturnObj.ScoreString = FormatForScoring(@(3, $OnePairCard, $TwoPairCard) + $RemainingCards)
        Return $ReturnObj
    }

    # One pair = isOnePair
    if ($isOnePair)
    {
        $ReturnObj.Desc = "One pair"
        $ReturnObj.ScoreString = FormatForScoring(@(2, $OnePairCard) + $RemainingCards)
        Return $ReturnObj
    }
    
    # High card only
    $ReturnObj.Desc = "High card"
    $ReturnObj.ScoreString = FormatForScoring(@(1) + $RemainingCards)
    
    Return $ReturnObj
}

$PlayerWinTable = @{}

# For each line / game from STDIN
foreach ($fileline in $input)
{
    # Reset variables for each line / game
    $PlayerArray = @()
    $CardArray = $fileline.Split()

    # Grab 5 Cards as a Hand for a Player
    while($CardArray.Count -ge 5)
    {
        # Process the Hand
        $PlayerArray += (@{
            PlayerNum = ($PlayerArray.Length+1) 
            Hand = $CardArray[0..4]; 
            ScoreObj = ScoreHand($CardArray[0..4]);
        })

        # Remove processed Cards
        $CardArray = $CardArray[5..($CardArray.Count-1)]
    }

    $WinningPlayer = 0
    $WinningPlayerScoreString = 0

    foreach ($Player in $PlayerArray)
    {
        # Verbose output with Hand, Description and Score String
        Write-Verbose "Player $($Player.PlayerNum): $($Player.Hand) - $($Player.ScoreObj.Desc) - [$($Player.ScoreObj.ScoreString)]"
        if (($WinningPlayer -eq 0) -or ($Player.ScoreObj.ScoreString -gt $WinningPlayerScoreString))
        {
            $WinningPlayer = $Player.PlayerNum
            $WinningPlayerScoreString = $Player.ScoreObj.ScoreString
        }
    }

    # Add a win to the winning player's tally
    $PlayerWinTable[$WinningPlayer]++

}

# Output overall wins tally for each Player
foreach ($Win in $PlayerWinTable.GetEnumerator() | Sort Name)
{
    Write-Host "Player $($Win.Name): $($Win.Value) hands"
}
