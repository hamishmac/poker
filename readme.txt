Depending on security settings, running this script may require a temporary execution policy change.

The example below changes the execution policy for the current process only.

1. Start PowerShell
2. cd \wherever_the_script_is
3. Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted
4. type .\poker-hands.txt | .\poker.ps1